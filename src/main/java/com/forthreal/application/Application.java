/* Developed by Lev Vanyan <lev.vanyan@forthreal.com> */
package com.forthreal.application;

import com.forthreal.application.configuration.TextConfigFiles;
import com.forthreal.application.v1.service.email.EmailSender;

public class Application
{
    private static String mailConfig = "settings.json";

    public static void main(String[] main)
    {
        TextConfigFiles.setConfigFile( "server.settings-dir", System.getProperty("user.dir") );
        TextConfigFiles.setConfigFile("server.emailsender", mailConfig );

        EmailSender sender = EmailSender.initObject();

        sender.sendEmail(
                "lev.vanyan@xcs.com.ua",
                "vinstance@gmail.com",
                "Parameters that you requested",
                "Lev, " + System.lineSeparator() + "as you previously asked, here's the test"
                );
    }
}
