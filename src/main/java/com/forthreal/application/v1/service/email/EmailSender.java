/* Developed by Lev Vanyan <lev.vanyan@forthreal.com> */
package com.forthreal.application.v1.service.email;

import com.alibaba.fastjson.JSONObject;
import com.forthreal.application.configuration.TextConfigFiles;
import com.forthreal.application.exception.AppException;
import com.forthreal.application.exception.SneakyException;
import com.forthreal.application.service.Holder;
import org.javatuples.Pair;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class EmailSender
{
    private Session mailSession = null;
    private Properties prop = null;

    private JSONObject smtpServerSettings = null;
    private JSONObject smtpAuthenticationSettings = null;
    private JSONObject securityOptions = null;

    private EmailSender() { }

    private void checkOptions(JSONObject configOptions, List<Pair<String, Class<?>>> optionsToCheck)
    {

        configOptions.keySet().forEach(
                key ->
                {
                    Holder<Boolean> hFound = new Holder<Boolean>(false);

                    optionsToCheck.forEach(
                            pair -> {

                                /* key found, let's compare the object type */
                                if( pair.getValue0().contains( key ) == true )
                                {
                                    if( pair.getValue1().equals( String.class ) )
                                    {
                                        String val = configOptions.getString( key );

                                        if(val == null)
                                        {
                                            SneakyException.sneakyThrow(
                                                    new AppException(
                                                            "Parameter " + key + " isn't " + pair.getValue1().getSimpleName()
                                                    )
                                            );
                                        }
                                    }
                                    else if( pair.getValue1().equals( Integer.class ) )
                                    {
                                        Integer val = configOptions.getInteger( key );

                                        if(val == null)
                                        {
                                            SneakyException.sneakyThrow(
                                                    new AppException(
                                                            "Parameter " + key + " isn't " + pair.getValue1().getSimpleName()
                                                    )
                                            );
                                        }
                                    }
                                    else if( pair.getValue1().equals( Boolean.class ) )
                                    {
                                        Boolean val = configOptions.getBoolean( key );

                                        if(val == null)
                                        {
                                            SneakyException.sneakyThrow(
                                                    new AppException(
                                                            "Parameter " + key + " isn't " + pair.getValue1().getSimpleName()
                                                    )
                                            );
                                        }
                                    }
                                }
                            }
                    );
                }
        );

    }

    public static EmailSender initObject()
    {
        EmailSender sender = new EmailSender();

        Pair<Boolean, JSONObject> pConfigObject =
                TextConfigFiles.getConfigContents( "server.emailsender" );

        if( pConfigObject.getValue0() == true )
        {
            JSONObject mailConfig = pConfigObject.getValue1();

            /****** SMTP AUTHENTICATION OPTIONS *******/
            sender.smtpAuthenticationSettings =
                    mailConfig.getJSONObject("smtpAuthenticationSettings");

            if( sender.smtpAuthenticationSettings == null )
            {
                SneakyException.sneakyThrow(
                        new AppException("smtpAuthenticationSettings not found in the mail configuration")
                    );
            }
            /* list what we're checking */
            List<Pair<String, Class<?>>> authOptions =
                Arrays.asList(
                    Pair.with("serverUsername", String.class),
                    Pair.with("serverPassword", String.class)
                );

            sender.checkOptions( sender.smtpAuthenticationSettings, authOptions );

            /****** SERVER ADDRESS / PORT OPTIONS *******/
            sender.smtpServerSettings =
                    mailConfig.getJSONObject("smtpServerSettings");

            if( sender.smtpServerSettings == null )
            {
                SneakyException.sneakyThrow(
                        new AppException("smtpServerSettings not found in the mail configuration")
                    );
            }

            /* list what we're checking */
            List<Pair<String, Class<?>>> serverOptions =
                    Arrays.asList(
                            Pair.with("serverHostName", String.class),
                            Pair.with("serverPort", Integer.class)
                    );

            sender.checkOptions( sender.smtpServerSettings, serverOptions );

            /****** SECURITY OPTIONS *******/
            sender.securityOptions =
                    mailConfig.getJSONObject("securityOptions");
            /* list what we're checking */
            List<Pair<String, Class<?>>> securityOptions =
                    Arrays.asList(
                            Pair.with("smtpAuthentication", Boolean.class),
                            Pair.with("smtpUseTLS", Boolean.class),
                            Pair.with("smtpUseSSL", Boolean.class)
                    );

            if( sender.securityOptions == null )
            {
                SneakyException.sneakyThrow(
                        new AppException("smtpOptions not found in the mail configuration")
                );
            }

            sender.checkOptions( sender.securityOptions, securityOptions );

        }
        else
        {
            SneakyException.sneakyThrow( new AppException("Unable to get mail config from \"server.emailsender\"") );
        }

        sender.setupSender();

        return sender;
    }

    protected void setupSender()
    {
        boolean hasSsl = true;
        boolean hasTLS = false;
        String smtpPrefix = "smtp";
        prop = new Properties();

        boolean smtpUseTLS = securityOptions.getBoolean("smtpUseTLS");
        boolean smtpUseSSL = securityOptions.getBoolean("smtpUseSSL");
        boolean smtpAuth = securityOptions.getBoolean("smtpAuthentication");

        if( smtpUseTLS == true )
        {
            hasSsl = false;
            prop.put("mail.smtp.starttls.enable", true);
            prop.put("mail.smtp.starttls.required", true);
        }

        if( smtpUseSSL == true )
        {
            if(hasSsl == false)
            {
                System.err.println(
                        "smtpUseSSL is set to true, but it doesn't work simultaneously with smtpUseTLS." +
                        " smtpUseTLS will prevail."
                    );
            }
            else
            {
                hasSsl = true;
                smtpPrefix = "smtps";
                prop.put("mail.smtp.ssl.enable", true );
            }
        }
        else
        {
            hasSsl = false;
        }

        if( smtpAuth == true )
        {
            prop.put("mail." + smtpPrefix + ".auth", true );
        }

        prop.put("mail." + smtpPrefix + ".host", smtpServerSettings.getString("serverHostname") );
        prop.put("mail." + smtpPrefix + ".port", smtpServerSettings.getString("serverPort") );

        if( smtpAuth == true )
        {
            String serverUsername = smtpAuthenticationSettings.getString("serverUsername");
            String serverPassword = smtpAuthenticationSettings.getString("serverPassword");

            mailSession =
                Session.getDefaultInstance
                        (
                                prop,
                                new Authenticator() {
                                    @Override
                                    protected PasswordAuthentication getPasswordAuthentication() {
                                        return new PasswordAuthentication(serverUsername,serverPassword);
                                    }
                                }
                        );
        }
        else
        {
            mailSession = Session.getInstance( prop );
        }
        mailSession.setDebug( true );

    }

    public Pair<Boolean, String> sendEmail
       (
          String emailFrom,
          String emailTo,
          String subjectText,
          String bodyText
       )
    {
        try
        {
            Message message = new MimeMessage( mailSession );

            message.setFrom( new InternetAddress(emailFrom) );
            message.setRecipient( Message.RecipientType.TO, new InternetAddress(emailTo) );
            message.setSubject( subjectText );

            MimeBodyPart mimeBody = new MimeBodyPart();
            mimeBody.setContent( bodyText, "text/plain");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart( mimeBody );

            message.setContent( multipart );

            Transport transport = null;

            if( securityOptions.getBoolean("smtpUseSSL") == true )
            {
                transport = mailSession.getTransport("smtps");
            }
            else
            {
                transport = mailSession.getTransport("smtp");
            }

            transport.connect();

            transport.send( message );
       }
        catch (Exception exc)
        {
            exc.printStackTrace();

            return Pair.with(false, exc.getLocalizedMessage() );
        }

        return Pair.with(true, "");
    }
}
