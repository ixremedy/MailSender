/* Developed by Lev Vanyan <lev.vanyan@forthreal.com> */
package com.forthreal.application.service;

public class Holder <T extends Object>
{
    public T value;

    public Holder(T value)
    {
        this.value = value;
    }

}

