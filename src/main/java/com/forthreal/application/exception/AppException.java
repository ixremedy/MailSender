/* Developed by Lev Vanyan <lev.vanyan@forthreal.com> */
package com.forthreal.application.exception;

public class AppException extends Exception
{
    private String message;

    public AppException(String message)
    {
        this.message = message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return this.message;
    }

}
