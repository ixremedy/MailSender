/* Developed by Lev Vanyan <lev.vanyan@forthreal.com> */
package com.forthreal.application.exception;

public class SneakyException
{

    public static <T extends Throwable> void sneakyThrow(Throwable e) throws T
    {
        Exception exc = (Exception) e;
        exc.printStackTrace();

        throw (T) e;
    }
}