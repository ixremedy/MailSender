/* Developed by Lev Vanyan <lev.vanyan@forthreal.com> */
package com.forthreal.application.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.javatuples.Pair;

import com.alibaba.fastjson.JSONObject;

public class TextConfigFiles
{
    private Map<String, String> configFilePaths = null;
    static private TextConfigFiles configFiles = null;

    private TextConfigFiles()
    {
        configFilePaths = new HashMap<>();
    }

    private static void checkEntity()
    {
        if( configFiles == null )
        {
            configFiles = new TextConfigFiles();
        }

    }

    static public String getConfigFile(String configFile)
    {
        checkEntity();

        return configFiles.configFilePaths.get( configFile );
    }

    static public void setConfigFile(String configFile, String path)
    {
        checkEntity();

        configFiles.configFilePaths.put( configFile, path );
    }

    static public TextConfigFiles getInstance()
    {
        checkEntity();

        return configFiles;
    }

    static public Pair<Boolean, JSONObject> getConfigContents(String configFile)
    {
        checkEntity();

        String configDirectory = configFiles.configFilePaths.get( "server.settings-dir" );
        String configName = configFiles.configFilePaths.get( configFile );

        boolean hasConfigFile = ( configName != null ) && ( configName.isEmpty() == false );
        boolean hasConfigDir = ( configDirectory != null ) && ( configDirectory.isEmpty() == false );

        if( ( hasConfigFile == true ) && ( hasConfigDir == true ) )
        {
            String filename = configDirectory + File.separatorChar + configName;

            try
            {
                BufferedReader reader = Files.newBufferedReader( Paths.get( filename ), StandardCharsets.UTF_8 );

                JSONObject configObject =
                        JSONObject.parseObject( reader.lines().collect( Collectors.joining() ) );

                if( configObject != null )
                {
                    return Pair.with( true, configObject );
                }

            }
            catch(Exception exc)
            {
                System.err.println(">>> Can't read \"" + configFile + "\": \"" + filename + "\". Exception: " + exc.getMessage() );
            }
        }

        return Pair.with( false, null );
    }
}
