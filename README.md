For those who need help in writing code sending out emails, here's a working sample. Change the settings in settings.json and it should work.
For _Gmail users_ wanting to use that smtp, you've got to enable "Less secure applications" for your mail profile for that to work.
